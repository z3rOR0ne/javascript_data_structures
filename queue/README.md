Queue: Generate Binary Numbers from 1 to n

Problem statement Implement a function findBin(n), which will generate binary numbers from 1 to n in the form of a string using a queue.

Input: A positive integer numbers

n = 3

Output: Returns binary numbers in the form of strings from 1 up to n

result = ["1","10","11"];

The easiest way to solve this problem is using a queue to generate new numbers from previous numbers.

Invoke using node index.js
