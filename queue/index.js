"use strict";
const Queue = require("./Queue.js");
function findBin(n) {
    let result = [];
    let myQueue = new Queue();
    let s1, s2;
    myQueue.enqueue("1");
    for (let i = 0; i < n; i++) {

        result.push(myQueue.dequeue());
        s1 = result[i] + "0";
        s2 = result[i] + "1";

        myQueue.enqueue(s1);
        myQueue.enqueue(s2);
    }

    return result;
}

console.log(findBin(10));

// Outputs:
// [
// '1', '10', '11',
// '100', '101', '110',
// '111', '1000', '1001',
// '1010'
// ]

