Graph: Remove Edge

Problem Statement: Implement the removeEdge function to take a source and a destination as arguments. It should detect if an edge exists between them.

Input: A graph, a source, and a destination

----------------------------------------------------------------------
|           Vertex               |               Edges               |
----------------------------------------------------------------------
|              0                 |                1,2                |
|              1                 |                 3                 |
|              2                 |                3,4                |
|              3                 |                null               |
|              4                 |                 0                 |
----------------------------------------------------------------------

Output: A graph with the edge between the source and the destination removed.

removeEdge(graph, 2, 3)

----------------------------------------------------------------------
|           Vertex               |               Edges               |
----------------------------------------------------------------------
|              0                 |                1,2                |
|              1                 |                 3                 |
|              2                 |                 4                 |
|              3                 |                null               |
|              4                 |                 0                 |
----------------------------------------------------------------------

The solution to this problem is fairly simple: we use indexing and deletion.

invoke using index.js

node index.js

