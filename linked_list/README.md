Linked List: Reverse a linked list

Problem statement: Write the reverse function to take a singly linked list and reverse it in place.

Input: a singly linked list

LinkedList = 0->1->2->3-4

Output: a reverse linked list

LinkedList = 4->3->2->1->0

The easiest way to solve this problem is by using iterative pointer manipulation.

invoke with index.js

node index.js
