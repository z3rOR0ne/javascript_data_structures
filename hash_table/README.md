Hash Table: Convert Max-Heap to Min-Heap

Problem statement: Implement the function convertMax(maxHeap) to convert a binary max-heap into a binary min-heap. maxHeap should be an array in the maxHeap format , i.e. the parent is greater than its children.

Input: a Max-Heap

maxHeap = [9,4,7,1,-2,6,5]

OutPut: returns the converted array

result = [-2,1,5,9,4,6,7]

To solve this problem we must min heapify all parent nodes.

To invoke, use index.js

node index.js


