Tree: Find the Minimum Value in a Binary Search Tree

Problem statement: Use the findMin(root) function to find the minimum value in a Binary Search Tree.

Input: a root node for a binary search tree

bst = {
    6 -> 4,9
    4 -> 2,5
    9 -> 8,12
    12 -> 10,14
}
where parent -> leftChild,rightChild

Output: the smallest integer value from that binary search tree

2

Solution: iterative findMin()

This solution begins by checking if the root is null. It returns null if so. It then moves to the left subtree and continues with each node's left child until the left-most child is reached.
invoke using index.js

node index.js
