// Array: Remove all even integers from an array
// Input: An array of random integers:
// [1,2,4,5,10,6,3]
// Output: An array containing only odd integers
// [1,5,3]

// Solution #1: Doing it "by hand"

function removeEven(arr) {
    const odds = []
    for (let number of arr) {
        if (number % 2 != 0) // Check if the item in the list is NOT even ('%' is the modulus symbol)
            odds.push(number) // If it isn't even, append it to the empty list
    }
    return odds // Return the new list
}
console.log(removeEven([3, 2, 41, 3, 34]))
// Output:
// [ 3, 41, 3 ]
